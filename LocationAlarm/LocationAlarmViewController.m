//
//  LocationAlarmViewController.m
//  LocationAlarm
//
//  Created by 大井 純 on 2013/04/14.
//  Copyright (c) 2013年 大井 純. All rights reserved.
//

#import "LocationAlarmViewController.h"
#import "MyAnnotation.h"

@interface LocationAlarmViewController ()

@end

@implementation LocationAlarmViewController
@synthesize longitude;
@synthesize latitude;
@synthesize userInfomaion;
@synthesize myMap;

//ログインビューを表示する
- (void)showLoginView
{
    // Create the log in view controller
    PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
    [logInViewController setDelegate:self];
    // Create the sign up view controller
    PFSignUpViewController *signUpViewController = [[PFSignUpViewController alloc] init];
    [signUpViewController setDelegate:self];
    // Assign our sign up controller to be displayed from the login controller
    [logInViewController setSignUpController:signUpViewController];
    
    // Present the log in view controller
    [self presentViewController:logInViewController animated:YES completion:NULL];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //
    NSLog(@"LocationAlarmViewController viewDidAppear");
    if (![PFUser currentUser]) {
        NSLog(@"LocationAlarmViewController user not logined.");
        [self showLoginView];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"LocationAlarmViewController viewDidLoad");
    
    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
        NSLog(@"latitude:%f longitude:%f", [geoPoint latitude], [geoPoint longitude]);
        latitude.text = [NSString stringWithFormat:@"%f", [geoPoint latitude]];
        longitude.text = [NSString stringWithFormat:@"%f", [geoPoint longitude]];
        CLLocationCoordinate2D loc;
        loc.latitude = [geoPoint latitude];
        loc.longitude = [geoPoint longitude];
        [myMap setCenterCoordinate:loc animated:YES];
        
        MKCoordinateRegion cr = myMap.region;
        cr.center = loc;
        cr.span.latitudeDelta = 0.005;
        cr.span.longitudeDelta = 0.005;
        
        [myMap setRegion:cr animated:YES];
        
        MyAnnotation *annotation;
        annotation = [annotation initWithCoordinate:loc];
        annotation.title = @"現在地";
        annotation.subtitle = @"ふが";
        //[myMap addAnnotation:annotation];
        
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password
{
    // Check if both fields are completed
    if (username && password && username.length != 0 && password.length != 0) {
        return YES; // Begin login process
    }
    
    [[[UIAlertView alloc] initWithTitle:@"ちゃんとして"
                                message:@"ユーザID、パスワードが入力されてません！"
                               delegate:nil
                      cancelButtonTitle:@"はい、ちゃんとします"
                      otherButtonTitles:nil] show];
    return NO; // Interrupt login process
}

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    userInfomaion.text = [NSString stringWithFormat:@"%@またお前か", [user username]];
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    NSLog(@"Failed to log in...");
}

// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    [self.navigationController popViewControllerAnimated:YES];
}

// Sent to the delegate to determine whether the sign up request should be submitted to the server.
- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info {
    BOOL informationComplete = YES;
    
    // loop through all of the submitted data
    for (id key in info) {
        NSString *field = [info objectForKey:key];
        if (!field || field.length == 0) { // check completion
            informationComplete = NO;
            break;
        }
    }
    
    // Display an alert if a field wasn't completed
    if (!informationComplete) {
        [[[UIAlertView alloc] initWithTitle:@"ちゃんとして"
                                    message:@"入力項目が全部入力されてません！"
                                   delegate:nil
                          cancelButtonTitle:@"はい、ちゃんとします"
                          otherButtonTitles:nil] show];
    }
    
    return informationComplete;
}

// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL]; // Dismiss the PFSignUpViewController
}

// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error {
    NSLog(@"Failed to sign up...");
}

// Sent to the delegate when the sign up screen is dismissed.
- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController {
    NSLog(@"User dismissed the signUpViewController");
}

- (IBAction)logoff:(id)sender {
    NSLog(@"Logout click");
    [PFUser logOut];
    [self showLoginView];
}

- (MKAnnotationView*)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKPinAnnotationView *pav = (MKPinAnnotationView*) [self.myMap dequeueReusableAnnotationViewWithIdentifier:@"myPin"];
    pav = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myPin"];
    pav.animatesDrop = YES;
    pav.pinColor = MKPinAnnotationColorPurple;
    return pav;
}

@end
