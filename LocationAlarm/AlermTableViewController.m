//
//  AlermTableViewController.m
//  LocationAlarm
//
//  Created by 大井 純 on 2013/07/22.
//  Copyright (c) 2013年 大井 純. All rights reserved.
//

#import "AlermTableViewController.h"
#import "SelectLocationViewController.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation AlermTableViewController

@synthesize selectedObj;
@synthesize isTimerExecuting;
@synthesize currentUser;

-(id)initWithCoder:(NSCoder *)aDecoder {
    NSLog(@"initWithCoder");
    self = [super initWithCoder:aDecoder];
    if (self) {
        currentUser = [PFUser currentUser];
        self.parseClassName = @"Alerm";
        self.textKey = @"address";
        self.pullToRefreshEnabled = YES;
        self.paginationEnabled = YES;
        self.objectsPerPage = 5;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(onTimer) userInfo:nil repeats:YES];
    isTimerExecuting = false;
}

- (void)onTimer
{
    NSLog(@"onTimer");
    if (!isTimerExecuting) {
        isTimerExecuting = true;
        [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
            PFQuery *query = [PFQuery queryWithClassName:@"Alerm"];
            [query whereKey:@"location" nearGeoPoint:geoPoint withinKilometers:0.1];
            [query whereKey:@"username" equalTo:currentUser.username];
            
            query.limit = 1;
            NSArray *locations = [query findObjects];
            
            NSLog([NSString stringWithFormat:@"location count:%d", [locations count]]);
            
            if ([locations count] > 0) {
                
                PFObject *obj = [locations objectAtIndex:0];
                
                AudioServicesPlaySystemSound(1010);
                
                NSString *msg = [NSString stringWithFormat:@"%@", [obj objectForKey:@"address"]];
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"ロケーションアラーム" message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
                [obj delete];
            }
            
            [self loadObjects];
            
            isTimerExecuting = false;
        }];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    // This method is called every time objects are loaded from Parse via the PFQuery
}

- (void)objectsWillLoad {
    [super objectsWillLoad];
    
    // This method is called before a PFQuery is fired to get more objects
}

- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query whereKey:@"username" equalTo:currentUser.username];
    
    [query orderByDescending:@"createdAt"];
    
    return query;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didSelectRowAtIndexPath");
    
    selectedObj = [[self objects] objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"showAlerm" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"prepareForSegue");
    NSLog(segue.identifier);
    if ([segue.identifier isEqualToString:@"showAlerm"]) {
        SelectLocationViewController *ctrl = (SelectLocationViewController*)[segue destinationViewController];
        ctrl.selectedObj = self.selectedObj;
    }
    else {
        SelectLocationViewController *ctrl = (SelectLocationViewController*)[segue destinationViewController];
        ctrl.selectedObj = nil;
    }
}

// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the first key in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    UITextView *address;
    
    address = (UITextView*)[cell viewWithTag:9];
    
    if (address == nil) {
        // Configure the cell
        UITextView *address = [[UITextView alloc] initWithFrame:CGRectMake(35, 10, 250, 40)];
        address.tag = 9;
        address.text = [object objectForKey:@"address"];
        address.editable = FALSE;
        [cell addSubview:address];
        [cell sendSubviewToBack:address];
    }
    else {
        address.text = [object objectForKey:@"address"];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

//Logoff btn action
- (IBAction)logoff:(id)sender {
    NSLog(@"Logoff.");
    [PFUser logOut];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"MainStoryboard" bundle:nil];
    UIViewController *loginView = [storyboard instantiateViewControllerWithIdentifier:@"loginView"];
    [self presentViewController:loginView animated:YES completion:nil];
}

- (IBAction)addAlerm:(id)sender {
}

@end
