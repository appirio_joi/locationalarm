//
//  main.m
//  LocationAlarm
//
//  Created by 大井 純 on 2013/04/14.
//  Copyright (c) 2013年 大井 純. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LocationAlarmAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LocationAlarmAppDelegate class]));
    }
}
