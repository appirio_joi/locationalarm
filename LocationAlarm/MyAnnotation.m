//
//  MyAnnotation.m
//  LocationAlarm
//
//  Created by 大井 純 on 2013/04/21.
//  Copyright (c) 2013年 大井 純. All rights reserved.
//

#import "MyAnnotation.h"

@implementation MyAnnotation

@synthesize coordinate, subtitle, title;

- (id) initWithCoordinate:(CLLocationCoordinate2D)c {
    coordinate = c;
    return self;
}

@end
