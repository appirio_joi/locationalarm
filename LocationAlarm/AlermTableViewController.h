//
//  AlermTableViewController.h
//  LocationAlarm
//
//  Created by 大井 純 on 2013/07/22.
//  Copyright (c) 2013年 大井 純. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface AlermTableViewController : PFQueryTableViewController
- (IBAction)logoff:(id)sender;
- (IBAction)addAlerm:(id)sender;

@property PFObject *selectedObj;
@property Boolean isTimerExecuting;
@property PFUser *currentUser;
@end
