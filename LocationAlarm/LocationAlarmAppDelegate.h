//
//  LocationAlarmAppDelegate.h
//  LocationAlarm
//
//  Created by 大井 純 on 2013/04/14.
//  Copyright (c) 2013年 大井 純. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationAlarmAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
