//
//  SelectLocationViewController.h
//  LocationAlarm
//
//  Created by 大井 純 on 2013/07/22.
//  Copyright (c) 2013年 大井 純. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <MapKit/MapKit.h>

@interface SelectLocationViewController : UIViewController
@property (weak, nonatomic) IBOutlet MKMapView *myMap;
- (IBAction)longPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *address;

- (IBAction)save:(id)sender;

@property PFObject *selectedObj;

@end
