//
//  LocationAlarmViewController.h
//  LocationAlarm
//
//  Created by 大井 純 on 2013/04/14.
//  Copyright (c) 2013年 大井 純. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <MapKit/MapKit.h>

@interface LocationAlarmViewController : UIViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, MKMapViewDelegate> {
}

@property (weak, nonatomic) IBOutlet UITextField *latitude;
@property (weak, nonatomic) IBOutlet UITextField *longitude;
@property (weak, nonatomic) IBOutlet UILabel *userInfomaion;
- (IBAction)logoff:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *myMap;

@end
