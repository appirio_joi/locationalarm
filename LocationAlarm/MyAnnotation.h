//
//  MyAnnotation.h
//  LocationAlarm
//
//  Created by 大井 純 on 2013/04/21.
//  Copyright (c) 2013年 大井 純. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyAnnotation : NSObject <MKAnnotation> {
    CLLocationCoordinate2D coordinate;
    NSString* subtitle;
    NSString* title;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain) NSString* subtitle;
@property (nonatomic, retain) NSString* title;

- (id) initWithCoordinate:(CLLocationCoordinate2D) coordinate;

@end
