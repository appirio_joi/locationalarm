//
//  SelectLocationViewController.m
//  LocationAlarm
//
//  Created by 大井 純 on 2013/07/22.
//  Copyright (c) 2013年 大井 純. All rights reserved.
//

#import "SelectLocationViewController.h"
#import "AlermTableViewController.h"

@interface SelectLocationViewController ()

@end

@implementation SelectLocationViewController

@synthesize myMap;
@synthesize address;
@synthesize selectedObj;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!selectedObj) {
        [self pointCurrentLocation];
    }
    else {
        PFGeoPoint *location = [selectedObj objectForKey:@"location"];
        [self pointLocation:location.latitude longitude:location.longitude];
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        annotation.coordinate = CLLocationCoordinate2DMake(location.latitude, location.longitude);
        [myMap addAnnotation:annotation];
        
        address.text = [selectedObj objectForKey:@"address"];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)longPressed:(id)sender {
    NSLog(@"Long Pressed");
    
    [myMap removeAnnotations:myMap.annotations];
    
    CGPoint touchedPoint = [sender locationInView:myMap];
    CLLocationCoordinate2D touchCoordinate = [myMap convertPoint:touchedPoint toCoordinateFromView:myMap];
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = touchCoordinate;
    [myMap addAnnotation:annotation];
    
    MKReverseGeocoder* reverseGeocoder = [[MKReverseGeocoder alloc] initWithCoordinate:touchCoordinate];
    reverseGeocoder.delegate = self;
    [reverseGeocoder start];
}

- (void)reverseGeocoder:(MKReverseGeocoder*)geocoder didFindPlacemark:(MKPlacemark*)placemark {
    // 住所情報取得が成功した場合
    address.text = placemark.title;
}

- (void) reverseGeocoder:(MKReverseGeocoder *)geocoder didFailWithError:(NSError*) error {
}

- (void)pointLocation: (double) latitude longitude:(double) longitude {
    
    NSLog(@"latitude:%f longitude:%f", latitude, longitude);

    CLLocationCoordinate2D loc;
    loc.latitude = latitude;
    loc.longitude = longitude;

    MKCoordinateRegion cr = myMap.region;
    cr.center = loc;
    cr.span.latitudeDelta = 0.005;
    cr.span.longitudeDelta = 0.005;
    
    [myMap setCenterCoordinate:loc animated:YES];
    
    [myMap setRegion:cr animated:YES];
    
}

- (void)pointCurrentLocation {
    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
        [self pointLocation:geoPoint.latitude longitude:geoPoint.longitude];
    }];
}

- (IBAction)save:(id)sender {
    PFUser *currentUser = [PFUser currentUser];
    
    NSArray *annotations = [myMap annotations];
    if ([annotations count] > 0) {
        MKPointAnnotation *annotation = [annotations objectAtIndex:0];
        
        PFGeoPoint *location = [PFGeoPoint geoPointWithLatitude:annotation.coordinate.latitude longitude:annotation.coordinate.longitude];
        
        PFObject *alerm;
        if (!selectedObj) {
            alerm = [PFObject objectWithClassName:@"Alerm"];
            [alerm setObject:currentUser.username forKey:@"username"];
            [alerm setObject:location forKey:@"location"];
            [alerm setObject:address.text forKey:@"address"];
        }
        else {
            alerm = selectedObj;
            [alerm setObject:location forKey:@"location"];
            [alerm setObject:address.text forKey:@"address"];
        }
        [alerm save];
        
        NSArray *allCtrl = self.navigationController.viewControllers;
        NSInteger index = [allCtrl count] - 2;
        AlermTableViewController *listCtrl = (AlermTableViewController*)[allCtrl objectAtIndex:index];
        [listCtrl loadObjects];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
