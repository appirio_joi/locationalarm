//
//  AlermEditViewController.m
//  LocationAlarm
//
//  Created by 大井 純 on 2013/07/22.
//  Copyright (c) 2013年 大井 純. All rights reserved.
//

#import "AlermEditViewController.h"

@interface AlermEditViewController ()

@end

@implementation AlermEditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
